#
# ~/.bash_profile
#

export PATH="${PATH}:${HOME}/bin"
tty-colorize

[[ -f ~/.bashrc ]] && . ~/.bashrc

ensure-ssh-agent

echo 'go done' >> ${HOME}/go-log
date >> ${HOME}/go-log

cd ~/linux-5.12.6
echo '' >> test
date >> test
gcc -o osmsg osmsg.c && \
    ./osmsg
dmesg >> test
emacs test --eval "(goto-char (point-max))"

#emacs +220 ~/linux-5.12.6/kernel/sys.c

# setting up light theme
source ${HOME}/.dircolors
