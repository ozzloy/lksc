#
# ~/.bashrc
#

echo "~/.bashrc"
# If not running interactively, don't do anything
[[ $- != *i* ]] && return
echo "~/.bashrc interactively"

alias ls='ls --color=auto'
PS1='[\u@\h \W]\$ '
