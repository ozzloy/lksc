#include <unistd.h>
#include <stdio.h>
#include <getopt.h>
#include <string.h>
#include <stdlib.h>

static char to[20]   = "";
static char msg[140] = "";
static char from[20] = "";

static struct option long_options[] =
  {
    {"send", required_argument, NULL, 's'},
    {"read", optional_argument, NULL, 'r'},
    {NULL, 0, NULL, 0}
  };

int send_msg(const char* to, const char* msg, const char* from){
  return syscall(443, to, msg, from);
}

int get_msg(const char* to, char* msg, char* from){
  return syscall(444, to, msg, from);
}

int main(int argc, char** argv){
  char choice;
  char action;
  int get_msg_result;
  strncpy(from, getenv("USER"), 20);
  while((choice = getopt_long(argc, argv, "s:r::",
			      long_options, NULL))
	!= -1)
    {
      switch (choice)
	{
	case 's':
	  strncpy(to, optarg, 20);
	  action = 's';
	  break;
	case 'r':
	  action = 'r';
	  strncpy(to, optarg? optarg : getenv("USER"), 20);
	}
    }
  if(action == 's' && optind + 1 == argc)
    {
      strncpy(msg, argv[optind++], 140);
      printf("\nsending to '%s', from '%s',\nmsg:\n%s",
	     to, from, msg);
      send_msg(to, msg, from);
    }
  else if(action == 'r')
    {
      printf("\nreading msgs to '%s'", to);
      while(1)
	{
	  get_msg_result = get_msg(to, msg, from);
	  if(0 < get_msg_result)
	    {
	      printf("\n\n  to: %s", to);
	      printf("\nfrom: %s", from);
	      printf("\n msg:\n%s", msg);
	    }
	  else if(0 == get_msg_result)
	    {
	      printf("\nno more messages");
	      break;
	    }
	  else
	    {
	      printf("\nget_msg problem occured! error %d",
		     get_msg_result);
	      return get_msg_result;
	    }
	}
    }
  else // bad command line arguments
    {
      printf("bad command line arguments!");
      if(optind < argc)
	{
	  printf("\nunhandled command line arguments:");
	  while(optind < argc)
	    {
	      printf("\n%s", argv[optind++]);
	    }
	}
    }
  putchar('\n');
  return 0;
}
